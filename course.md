<!--
author:   me

email:    me

version:  0.0.2

language: fr

narrator: French Male

import: https://raw.githubusercontent.com/liaTemplates/AVR8js/main/README.md

-->

# Testing LiaScript in a GitLab-Pipeline

## Executable Code

``` cpp
void setup() {
  Serial.begin(9600);
}

void loop() {
   while (Serial.available() > 0 ) {

     String str = Serial.readString();

     if (str.equals("send")) {
        Serial.println("identified");
     } else {
        Serial.println("unknown");
     }
   }
}
```
@AVR8js.sketch

## Movies and Audios

?[audio](https://bigsoundbank.com/UPLOAD/mp3/1068.mp3)

!?[video](https://www.youtube.com/watch?v=bICfKRyKTwE)

## Interactive tables 

<!-- data-transpose data-show -->
| Market Share of LMS in 2016 | Europe |
|:--------------------------- | ------:|
| Blackboard learn            |    192 |
| Canvas                      |     16 |
| Claroline                   |     32 |
| D2L Brightspace             |     16 |
| GUNET eClass                |     32 |
| Ilias                       |     64 |
| Itslearning                 |     32 |
| Moodle                      |   1043 |
| Olat                        |     17 |
| Others                      |     80 |
| Sakai                       |     48 |
| Stud.IP                     |     32 |

## Quizzes 

Just add as many points as you wish:

    [[X]] 1
    [[ ]] 2
    [[X]] 3
    [[?]] What is the characteristic of an even number?


## ASCII art 

<!--
style="width: 100%; max-width: 860px; display: block; margin-left: auto; margin-right: auto;"
-->
```ascii

Version 1.0                           Version 1.1
+---------------------------+          +---------------------------+
| Course  German Literatur  |          | Course  German Literature |
| Authors John Muster       | "Error"  | Authors John Muster       |
|                           |------->  |         Angelika Maier    |----->
|~~~~~~~~~~~~~~~~~~~~~~~~~~~|          |~~~~~~~~~~~~~~~~~~~~~~~~~~~|
| In 1756 Goethe visited    |---.      | In 1786 Goethe visited    |--.
| Italy ...                 |   |      | Italy ...                 |  |
                                |                                     |
                                |                                     |    +----------------------------+
                                |                                     |    | Course  Deutsche Literatur |
                                |                                     |    | Authors John Muster        |
                                |                                     .--> |         Angelika Maier     |
                                |                                          |         Steve Gray         |
                                |                                          |~~~~~~~~~~~~~~~~~~~~~~~~~~~~|
                                |                                          | 1786 reiste Goethe nach    |
                                |                                          | Italien ...                |
                                |       Version 1.0
                                |      +---------------------------+
                                |      | Course  Goethe & Schiller |
                                |      | Authors John Muster       |
                                .-->   |         Angelika Maier    |----->
                                       |~~~~~~~~~~~~~~~~~~~~~~~~~~~|
                                       | The correspondence during |
                                       | the Italian journey ...   |
```

